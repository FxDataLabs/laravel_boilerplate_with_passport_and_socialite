{{ Form::model($logged_in_user, ['route' => 'frontend.user.usertask.start', 'class' => 'form-horizontal', 'method' => 'POST']) }}

	<div class="form-group">
		<div class="col-md-8 col-md-pull-4">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">

                            <h4>{{ trans('labels.task.header') }}</h4>
                            
                        </div><!--panel-heading-->

                        <div class="panel-body">
                            @foreach($task_list as $task)
                                <p>{{ $task->task_name }}

                                    <!-- create a checkbox and make true if user has already checked -->
                                    {{ Form::checkbox('tasks[]', $task->id, in_array($task->id, $checkedTaskArray)) }}

                                </p>
                            @endforeach
                            
                            {{ Form::submit(trans('labels.task.save'), ['class' => 'btn btn-primary', 'id' => 'update-task']) }}

                        </div><!--panel-body-->
                    </div><!--panel-->
                </div><!--col-xs-12-->
            </div><!--row-->
        </div>
	</div>

{{ Form::close() }}
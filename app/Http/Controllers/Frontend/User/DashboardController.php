<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Access\User\Task;
use App\Models\Access\User\UserTask;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	$task_list = Task::all();
    	$checkedTaskList = UserTask::where('user_id','=',access()->id())->get();

    	$checkedTaskArray = [];
    	foreach ($checkedTaskList as $checkedTask) 
    	{
    		$checkedTaskArray[] = $checkedTask->task_id;
    	}

        return view('frontend.user.dashboard',compact(['task_list','checkedTaskArray']));
    }
}

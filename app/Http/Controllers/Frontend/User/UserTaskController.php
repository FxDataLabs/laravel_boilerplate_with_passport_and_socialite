<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Access\User\UserTask;
use Illuminate\Support\Facades\DB;

/**
 * Class UserTaskController.
 */
class UserTaskController extends Controller
{

    /**
     * @param UpdateTaskRequest $request
     *
     * @return mixed
     */
    public function start(Request $request)
    {
        $taskArray = $request->input('tasks');

        $dataSet = [];
        foreach ($taskArray as $task) 
        {
            $dataSet[] = [
                'task_id'  => $task,
                'user_id'  => access()->id(),
                'checked'  => 'Y',
            ];
        }

        DB::table('user_tasks')->insert($dataSet);

        return redirect()->route('frontend.user.dashboard')->withFlashSuccess(trans('strings.frontend.tasks.start'));
    }
}

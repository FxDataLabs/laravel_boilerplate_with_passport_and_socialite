## Laravel 5.4 Boilerplate

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) [![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) [![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://styleci.io/repos/30171828/shield?style=plastic)

### Official Documentation

[Click here for the official documentation](http://laravel-boilerplate.com)

### Introduction

Laravel Boilerplate with Passport provides you all the functionalities of Laravel Boilerplate with Passport (OAuth 2.0) and active socialite Login with google and facebook. you can also active other socialite by yourself just as google and facebook. This is provider and for testing we also attach the consumer with it for the newby in Laravel so they can understand easily.

Also Email verification is activated you just have to add your email, password and display name in .env file and it will send you a verification email when you register in website.

### How to use

- git clone https://FxDataLabs@bitbucket.org/FxDataLabs/laravel_boilerplate_with_passport-oauth2.0.git 

- add you website name in /etc/hosts and homestead.yml file. 

- vagrant up 

- <yourboilerplatewebsitename> in browser 

- Login in website and create a client from (Create New Client) 

- give any name and add http://<yourconsumerwebsitename>/callback in redirect url 

(This is simple Laravel Boilerplate with socialite login and email verification)


- git clone https://FxDataLabs@bitbucket.org/FxDataLabs/consumer_for_oauth.git 

- add you website name in /etc/hosts and homestead.yml file. 

- vagrant up 

- add <yourboilerplatewebsitename> in web.php in consumer project. 

- copy client_id and client_secret from <yourboilerplatewebsitename> and paste 

- <yourconsumerwebsitename> in browser  
	
(This consumer is used for Passport funcationlities where laravel_boilerplate_with_passport-oauth2.0 serve as a provider) 

### Wiki

Please view the [wiki](https://github.com/rappasoft/laravel-5-boilerplate/wiki) for a list of [features](https://github.com/rappasoft/laravel-5-boilerplate/wiki#features).

### Contributing

Please feel free to make any pull requests, or e-mail me a feature request you would like to see in the future to FxDataLabs @ contact@htree.plus
